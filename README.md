# README #

### This is a Kioske/Harry Potter where are you Clock application ###

* Kioske tells basic things about where people are currently
* version 1... I guess

### How do I get set up? ###

* Down load repo
* cd to directory
* Bundler should install everything (as long as you have bundler)
* Hook up Arduino, check which port it is comming in
  * If you are on Windows change the "COM6" to the appropriate COM number
  * If on linux, check your Arduino's port if /dev/ttyACM0 just remove "COM6"
  * Mac replace "COM6" with your port
* Currently there is no database, but SQLite will suffice when/if you make users
* Testing will commence... eventually

## Things you will need! ##

* a few servos based on who all you want to keep track of
* an Arduino Uno... Check arduino_firmata doc for other arduinos
* (optional) a solid state relay, just to control power to and from servos
* (optional) lEDs  cause who doesn't like lights!

### Contribution guidelines ###

* Writing tests probably should be done...
* Code review
