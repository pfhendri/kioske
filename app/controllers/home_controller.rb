class HomeController < ApplicationController
  def index

  end

  def setposition
    begin
      if params[:person] && params[:place] && params[:pin]
        person = params[:person]
        place = params[:place]
        pin = params[:pin].to_i
        @arduino = Arduino.new(person, place, pin)
      else
        flash[:notice] = "Oops!  It didn't work!"
      end
    rescue
      flash[:notice] = "The arduino needs reset... or reconnected..."
    end
    redirect_to root_path
  end
end
