class Arduino
  attr_accessor :person, :place, :pin #making specifics available outside of class

  def initialize(person, place, pin) #initializer to create new arduino
    @person = person
    @place = place
    @pin = pin
    @angle = 0

## Case change based on where you are going
    case place
    when "work"
      @angle = 100
    when "home"
      @angle = 225
    else
      @angle = 0
    end

## "COM#" to be set form Windows, leave blank for '/dev/ttyACM0' if on linux
    arduino = ArduinoFirmata.connect "COM6"
    1.times do
      arduino.digital_write 13, true
      arduino.servo_write @pin, @angle
      sleep 1
      arduino.digital_write 13, false
      arduino.close
    end

  end # end of initialize
end
